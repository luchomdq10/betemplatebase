﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Service.Dtos.Response;
using Service.Interface;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Service.GenerateToken
{
    public class TokenService : ITokenService
    {
        private readonly IConfiguration _configuration;

        public TokenService(IConfiguration configuration) 
        {
            _configuration = configuration;
        }
        public string GenerarTokenJWTAsync(UserResponse usuarioInfo)
        {
            // CREAMOS EL HEADER //
            var _symmetricSecurityKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(_configuration["JWT:ClaveSecreta"])
                );
            var _signingCredentials = new SigningCredentials(
                    _symmetricSecurityKey, SecurityAlgorithms.HmacSha256
                );
            var _Header = new JwtHeader(_signingCredentials);

            // CREAMOS LOS CLAIMS //
            var _Claims = new[] {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.NameId, usuarioInfo.UserId.ToString()),
                new Claim("nombre", usuarioInfo.Email),
                new Claim("apellidos", usuarioInfo.Password),
                new Claim(JwtRegisteredClaimNames.Email, usuarioInfo.Email),
            };

            JwtPayload _Payload = CreateJwtPayload(_Claims);

            JwtSecurityToken _Token = GenerateToken(_Header, _Payload);

            return new JwtSecurityTokenHandler().WriteToken(_Token);
        }

        private  JwtSecurityToken GenerateToken(JwtHeader _Header, JwtPayload _Payload)
        {
            // GENERAMOS EL TOKEN //
            return new JwtSecurityToken(
                    _Header,
                    _Payload
                );
        }

        private JwtPayload CreateJwtPayload(Claim[] _Claims)
        {
            // CREAMOS EL PAYLOAD //
            var _Payload = new JwtPayload(
                    issuer:"test",
                    audience: "test",
                    claims: _Claims,
                    notBefore: DateTime.UtcNow,
                    // Exipra a la 24 horas.
                    expires: DateTime.UtcNow.AddHours(24)
                );
            return _Payload;
        }
    }
}
