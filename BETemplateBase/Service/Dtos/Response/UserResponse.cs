﻿namespace Service.Dtos.Response
{
    public class UserResponse
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public UserResponse ToUserResponse(Entities.User userEntity)
        {
            if (userEntity != null)
            {
                return new UserResponse()
                {
                    Email = userEntity.Email,
                    Password = userEntity.Password,
                    UserId = userEntity.UserId
                };
            }

            return null;
        }
    }
}
