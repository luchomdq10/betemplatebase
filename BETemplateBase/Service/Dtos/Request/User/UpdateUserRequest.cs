﻿namespace Service.Dtos.Request
{
    public class UpdateUserRequest
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

    }
}
