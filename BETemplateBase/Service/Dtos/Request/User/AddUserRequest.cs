﻿namespace Service.Dtos.Request.User
{
    public class AddUserRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public Entities.User ToUser()
        {
            return new Entities.User()
            {
                Email = this.Email,
                Password = this.Password
            };
        }

    }
}
