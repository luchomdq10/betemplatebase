﻿namespace Service.Dtos.Request.Login
{
    public class LoginRequest
    {
        public string Usuario { get; set; }
        public string Password { get; set; }
    }
}
