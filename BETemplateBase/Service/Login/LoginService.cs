﻿using Helper;
using Helper.Exceptions;
using Repository.Interface.Query;
using Service.Dtos.Response;
using Service.Interface;
using System.Threading.Tasks;

namespace Service.Login
{
    public class LoginService : ILoginService
    {
        private readonly IUserQuery _userQuery;
        public LoginService(IUserQuery userQuery)
        {
            _userQuery = userQuery;
        }

        public async Task<UserResponse> AutenticarUsuarioAsync(string usuario, string password)
        {
            var user = await _userQuery.GetUserByEmailAndPasswordAsync(usuario, password);

            if(user is null) 
            {
                throw new GenericException(ErrorCode.Status400BadRequest, MessageGeneral.DontExist);
            }

            return DtoMapperExtension.MapTo<UserResponse>(user);
        }
    }
}
