﻿using Service.Dtos.Response;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface ILoginService
    {
        Task<UserResponse> AutenticarUsuarioAsync(string usuario, string password);
    }
}
