﻿using Service.Dtos.Response;

namespace Service.Interface
{
    public interface ITokenService
    {
        string GenerarTokenJWTAsync(UserResponse usuarioInfo);
    }
}
