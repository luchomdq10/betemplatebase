﻿using Service.Dtos.Request;
using Service.Dtos.Request.User;
using Service.Dtos.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface IUserService
    {
        Task<UserResponse> AddUserAsync(AddUserRequest userRequest);
        Task<UserResponse> GetUserByEmailAsync(string email);

        Task<UserResponse> GetUserByIdAsync(int id);

        Task<IEnumerable<UserResponse>> GetUserAsync();

        Task<UserResponse> UpdateUserAsync(UpdateUserRequest updateUserRequest);
    }
}
