﻿using Helper;
using Helper.Exceptions;
using Repository.Interface.Command;
using Repository.Interface.Query;
using Service.Dtos.Request;
using Service.Dtos.Request.User;
using Service.Dtos.Response;
using Service.Interface;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.User
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserQuery _userQuery;
        public UserService(IUserRepository userRepository, IUserQuery userQuery)
        {
            _userRepository = userRepository;
            _userQuery = userQuery;
        }
        public async Task<UserResponse> AddUserAsync(AddUserRequest userRequest)
        {
            
            var user = await GetUserByEmailAsync(userRequest.Email);

            if (user != null)
            {
                throw new GenericException(ErrorCode.Status400BadRequest,MessageGeneral.MessageExist);
            }

            Entities.User userEntity = userRequest.ToUser();

            var userCreated = await _userRepository.AddUserAsync(userEntity);

            var userResponse = DtoMapperExtension.MapTo<UserResponse>(userCreated);

            return userResponse;

        }

        public async Task<IEnumerable<UserResponse>> GetUserAsync()
        {
            var users =  await _userQuery.GetUser();

            var usersRpesonse = DtoMapperExtension.MapTo<IEnumerable<UserResponse>>(users);

            return usersRpesonse;
        }

        public async Task<UserResponse> GetUserByEmailAsync(string email)
        {
            var userEntity =  await _userQuery.GetUserByEmailAsync(email);
            
            if(userEntity is null) 
            {
                return null;
            }

            var userResponse = DtoMapperExtension.MapTo<UserResponse>(userEntity);

            return userResponse;
        }

        public async Task<UserResponse> GetUserByIdAsync(int id)
        {
            var userEntity = await _userQuery.GetUserById(id);

            var userResponse = DtoMapperExtension.MapTo<UserResponse>(userEntity);

            return userResponse;
        }

        public async  Task<UserResponse> UpdateUserAsync(UpdateUserRequest updateUserRequest)
        {
            var userEntity = await _userQuery.GetUserById(updateUserRequest.UserId);

            if(userEntity == null) 
            {
                throw new GenericException(ErrorCode.Status400BadRequest, MessageGeneral.MessageExist);
            }

            userEntity.Email = string.IsNullOrEmpty(updateUserRequest.Email) ? userEntity.Email : updateUserRequest.Email;
            userEntity.Password = string.IsNullOrEmpty(updateUserRequest.Password) ? userEntity.Password : updateUserRequest.Password;

            var userUpdated = await _userRepository.UpdateUserAsync(userEntity);

            var userResponse = DtoMapperExtension.MapTo<UserResponse>(userUpdated);

            return userResponse;
        }
    }
}
