﻿using BETemplateBase.Request;
using Helper.Exceptions;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Service.Interface;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Helper;

namespace BETemplateBase.Controllers.User
{
    [ApiController]
    [EnableCors("allowSpecificOrigins")]
    [Route("Users")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly string ENDPOINT_USER = "https://localhost:44318/User/{ID}";

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Route("")]
        public async Task<IActionResult> PostUserAsync([FromBody] AddUserRequest addUser)
        {
            try
            {
                var addUserRequest = addUser.ToUserRequest();

                var userResponse = await _userService.AddUserAsync(addUserRequest);

                return Ok(userResponse);

            }
            catch (GenericException ex)
            {
                return ErrorResponse.BuildErrorResponse(ex.ResponseMessage, ENDPOINT_USER,
                                                        "Users", ex.ErrorCode);
            }

        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Route("{id}")]
        public async Task<IActionResult> GetUserByIdAsync(int id)
        {
            try
            {
                var userResponse = await _userService.GetUserByIdAsync(id);

                return Ok(userResponse);

            }
            catch (GenericException ex)
            {
                return ErrorResponse.BuildErrorResponse(ex.ResponseMessage, ENDPOINT_USER,
                                                        "Users", ex.ErrorCode);
            }
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Route("")]
        public async Task<IActionResult> GetUserAsync()
        {
            try
            {
                var userResponse = await _userService.GetUserAsync();

                return Ok(userResponse);

            }
            catch (GenericException ex)
            {
                return ErrorResponse.BuildErrorResponse(ex.ResponseMessage, ENDPOINT_USER,
                                                        "Users", ex.ErrorCode);
            }
        }

        [HttpPatch]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Route("id")]
        public async Task<IActionResult> UpdateUserAsync([FromBody] UpdateUserRequest addUser, int id)
        {
            try
            {
                var updateUser = addUser.ToUpdateRequest(id);

                var  userResponse = await _userService.UpdateUserAsync(updateUser);
               
                return Ok(userResponse);

            }
            catch (GenericException ex)
            {
                return ErrorResponse.BuildErrorResponse(ex.ResponseMessage, ENDPOINT_USER,
                                                        "Users", ex.ErrorCode);
            }

        }
    }
}
