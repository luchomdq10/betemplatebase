﻿using BETemplateBase.Request;
using Helper;
using Helper.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Service.Interface;
using System.Threading.Tasks;

namespace BETemplateBase.Controllers.Login
{
    [ApiController]
    [EnableCors("allowSpecificOrigins")]
    [Route("Login")]
    public class LoginController : ControllerBase
    {
        private readonly ILoginService _loginService;
        private readonly ITokenService _tokenService;
        private readonly string ENDPOINT_LOGIN = "https://localhost:44318/Login";

        // TRAEMOS EL OBJETO DE CONFIGURACIÓN (appsettings.json)
        public LoginController(ILoginService loginService, ITokenService tokenService)
        {
            _loginService = loginService;
            _tokenService = tokenService;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginRequest loginRequest)
        {
            try 
            {
                var _userInfo = await _loginService.AutenticarUsuarioAsync(loginRequest.Usuario, loginRequest.Password);

                if (_userInfo != null)
                {
                    return Ok(new { token = _tokenService.GenerarTokenJWTAsync(_userInfo) });
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (GenericException ex)
            {
                return ErrorResponse.BuildErrorResponse(ex.ResponseMessage, ENDPOINT_LOGIN,
                                                        "Login", ex.ErrorCode);
            }
        }
    }
}
