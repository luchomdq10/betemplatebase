﻿using BETemplateBase.Request;
using Helper;
using Helper.Validator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.CrudComment.Get;
using Service.Dtos;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BETemplateBase.Controllers.Comment
{
    [ApiController]
    [EnableCors("allowSpecificOrigins")]
    [Route("Comments")]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService _commentService;
        private readonly string ENDPOINT_COMMENT = "https://localhost:44318/Comment/{ID}";

        IList<GetResponseComments> comments;
        CommentDto commentDto;
        bool result;
        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }
   
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Route("")]
        public async Task<IActionResult> PostCommentAsync([FromBody] AddCommentRequest addComment)        
        {
            try
            {
                var addCommentRequest = addComment.ToBuiderCommentRequest(addComment);

                await _commentService.AddComment(addCommentRequest);
              
            }
            catch (Exception ex)
            {
                NotFound(ex.Message);
            }

            return Ok();
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetCommentsAsync()
        {
            try
            {
                var listComment = await _commentService.GetCommentsAsync();

                comments = GetResponseComments.FromList(listComment);

            }
            catch(Exception ex)
            {
               return NotFound(ex.Message);
            }
            
            return Ok(comments);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Route("{commentId}")]
        public async Task<IActionResult> GetCommentByIdAsync(string commentId)
        {
            try
            {
                GetCommentByIdRequest commentByIdViewModel = new GetCommentByIdRequest(commentId);

                var getByIdrequest = commentByIdViewModel.BuilderRequest(commentByIdViewModel);

               commentDto = await _commentService.GetCommentByIdAsync(getByIdrequest.IdComment);

                if(commentDto is null)
                {
                    //var problemDetails = Helper.ProblemDetails.GetProblemDetails(ENDPOINT_COMMENT, "Get Product by ID", StatusCode, MessageGeneral.DontExist);

                    //return new ObjectResult(problemDetails)
                    //{
                    //    ContentTypes = { "application/problem+json" },
                    //    StatusCode = 400,
                    //};
                } 
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }

            return Ok(commentDto);
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Route("{commentId}")]
        public async Task<IActionResult> DeleteCommentByIdAsync(string commentId)
        {
            try
            {
                GetCommentByIdRequest commentByIdViewModel = new GetCommentByIdRequest(commentId);

                var getByIdrequest = commentByIdViewModel.BuilderRequest(commentByIdViewModel);

                result = await _commentService.DeleteByIdAsync(getByIdrequest.IdComment);

                if (!result)
                {
                    //var problemDetails = Helper.ProblemDetails.GetProblemDetails(ENDPOINT_COMMENT, "Get Product by ID", StatusCode, MessageGeneral.DontExist);

                    //return new ObjectResult(problemDetails)
                    //{
                    //    ContentTypes = { "application/problem+json" },
                    //    StatusCode = 400,
                    //};
                }
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }

            return Ok(MessageGeneral.DeleteSuccess);
        }

        [HttpPatch]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Route("{idComment}/updateComment")]
        public async Task<IActionResult> UpdateCommentAsync(string idComment,UpdateCommentRequest updateCommentViewModel)
        {
            try
            {
                updateCommentViewModel.IdComment = idComment;

                var updateCommentRequest = updateCommentViewModel.ToBuiderCommentRequest(updateCommentViewModel);

                commentDto = await _commentService.UpdateCommentAsync(updateCommentRequest);

                if (commentDto is null)
                {
                    //var problemDetails = Helper.ProblemDetails.GetProblemDetails(ENDPOINT_COMMENT, "Get Product by ID", 400, MessageGeneral.DontExist);

                    //return new ObjectResult(problemDetails)
                    //{
                    //    ContentTypes = { "application/problem+json" },
                    //    StatusCode = 400,
                    //};
                }
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }

            commentDto.IdComment = idComment.ToInt().Value;

            return Ok(commentDto);
        }
    }
}