﻿using Helper.Validator;
using Service.CrudComment.GetById;


namespace BETemplateBase.Request
{
    public class GetCommentByIdRequest
    {
        [IsIntValidator]
        public string IdCommment { get; set; }

        public GetCommentByIdRequest(string idComment)
        {
            IdCommment = idComment;
        }

        public GetByIdRequest BuilderRequest(GetCommentByIdRequest getCommentByIdView)
        {
            return new GetByIdRequest()
            {
                IdComment = getCommentByIdView.IdCommment.ToInt().Value
            };
        }
    }
}
