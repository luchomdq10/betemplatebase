﻿using Service.Dtos.Request.User;
using System.ComponentModel.DataAnnotations;

namespace BETemplateBase.Request
{
    public class AddUserRequest
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        public Service.Dtos.Request.User.AddUserRequest ToUserRequest() 
        {
            return new Service.Dtos.Request.User.AddUserRequest()
            {
                Email = this.Email,
                Password = this.Password
            };
        }
    }
}
