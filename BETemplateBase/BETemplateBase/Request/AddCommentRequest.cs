﻿using Service.CrudComment.Add;
using System.ComponentModel.DataAnnotations;

namespace BETemplateBase.Request
{
    public class AddCommentRequest
    {
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }
        public CommentRequest ToBuiderCommentRequest(AddCommentRequest addCommentViewModel)
        {
            return new CommentRequest()
            {
                Title = addCommentViewModel.Title,
                Description = addCommentViewModel.Description
            };
        }
    }
}
