﻿using S = Service.Dtos.Request;

namespace BETemplateBase.Request
{
    public class UpdateUserRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public S.UpdateUserRequest ToUpdateRequest(int id) 
        {
            return new S.UpdateUserRequest()
            {
                UserId = id,
                Email = this.Email,
                Password = this.Password
            };
        }
    }
}
