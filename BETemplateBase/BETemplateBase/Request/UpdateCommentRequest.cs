﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace BETemplateBase.Request
{
    public class UpdateCommentRequest
    {
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }

        [Required]

        public string Description { get; set; }

        [JsonIgnore]
        public string IdComment { get; set; }
        public Service.CrudComment.Update.UpdateCommentRequest ToBuiderCommentRequest(UpdateCommentRequest UpdateCommentViewModel)
        {
            return new Service.CrudComment.Update.UpdateCommentRequest()
            {
                IdComment = UpdateCommentViewModel.IdComment,
                Title = UpdateCommentViewModel.Title,
                Description = UpdateCommentViewModel.Description
            };
        }
    }
}
