﻿using System.ComponentModel.DataAnnotations;

namespace Helper.Validator
{
    public class IsDecimalValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var result = decimal.TryParse(value.ToString(), out _);

            if (!result)
            {
                return new ValidationResult(validationContext.DisplayName + MessageGeneral.InvalidFormat);
            }

            return ValidationResult.Success;
        }
    }
}
