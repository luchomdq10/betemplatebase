﻿using System.ComponentModel.DataAnnotations;

namespace Helper.Validator
{
    public class IsBoolValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var result = bool.TryParse(value.ToString(), out _);

            if (!result)
            {
                return new ValidationResult(validationContext.DisplayName + MessageGeneral.InvalidFormat);
            }

            return ValidationResult.Success;
        }
    }
}
