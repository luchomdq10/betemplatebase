﻿using System;


namespace Helper.Exceptions
{
    public class GenericException : Exception
    {
        public ErrorCode ErrorCode { get; set; }
        public string ResponseMessage { get; set; }

        public GenericException(ErrorCode errorCode, string responseMessage = null)
            : base(String.Format("ErrorCode: {0}", errorCode.ToString()))
        {
            ErrorCode = errorCode;
            ResponseMessage = responseMessage;
        }
    }
}
