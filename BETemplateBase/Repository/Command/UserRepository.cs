﻿using Entities;
using Repository.Configure;
using Repository.Interface.Command;
using System.Threading.Tasks;

namespace Repository.Command
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDBContext _dBContext;

        public UserRepository(ApplicationDBContext dBContext)
        {
            _dBContext = dBContext;
        }

        public async Task<User> AddUserAsync(User user)
        {
            var userResult = _dBContext.Users.Add(user);

            await _dBContext.SaveChangesAsync();

            return userResult.Entity;
        }

        public async Task<User> UpdateUserAsync(User user)
        {
            var userUpdated = _dBContext.Users.Update(user);
            await _dBContext.SaveChangesAsync();

            return userUpdated.Entity;
        }
    }
}
