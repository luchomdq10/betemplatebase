﻿using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Repository.Configure.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("Users");
            entityTypeBuilder.HasKey(e => e.UserId);
            entityTypeBuilder.Property(e => e.Email);
            entityTypeBuilder.Property(e => e.Password)
                .HasMaxLength(50);
        }
    }
}
