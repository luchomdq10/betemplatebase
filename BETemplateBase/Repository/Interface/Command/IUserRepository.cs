﻿using Entities;
using System.Threading.Tasks;

namespace Repository.Interface.Command
{
    public interface IUserRepository
    {
        Task<User> AddUserAsync(User user);
        Task<User> UpdateUserAsync(User user);
    }
}
