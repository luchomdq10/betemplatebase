﻿using Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repository.Interface.Query
{
    public interface IUserQuery
    {
        Task<User> GetUserByEmailAsync(string email);
        Task<User> GetUserById(int id);
        Task<IEnumerable<User>> GetUser();
        Task<User> GetUserByEmailAndPasswordAsync(string email, string password);
    }
}
