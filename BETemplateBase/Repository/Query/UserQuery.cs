﻿using Entities;
using Repository.Configure;
using Repository.Interface.Query;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Repository.Query
{
    public class UserQuery : IUserQuery
    {
        private ApplicationDBContext _applicationDBContext;
        public UserQuery(ApplicationDBContext applicationDBContext)
        {
            _applicationDBContext = applicationDBContext;
        }

        public async Task<IEnumerable<User>> GetUser()
        {
            return await _applicationDBContext.Users.ToListAsync();
        }

        public async Task<User> GetUserByEmailAsync(string email)
        {
           var user = await _applicationDBContext.Users.Where(x => x.Email == email).ToListAsync();

           return user.FirstOrDefault();
            
        }

        public async Task<User> GetUserById(int id) 
        {
            var user = await _applicationDBContext.Users.Where(x => x.UserId == id)
                                                        .FirstOrDefaultAsync();

            return user;
        }

        public async Task<User> GetUserByEmailAndPasswordAsync(string email,string password)
        {
            var user = await _applicationDBContext.Users.FirstOrDefaultAsync(x => x.Email == email && x.Password == password);

            return user;

        }
    }
}
